#!/bin/bash

TMP=$(mktemp -d)
ZIPURL=https://github.com/thsmi/sieve/releases/download/0.4.0/sieve-linux-x64.zip
ZIPNAME=$(basename $ZIPURL)
BIN=sieve
BINDEST=/usr/local/lib/$BIN
APP=sieve.desktop
APPDEST=/usr/share/applications 

cd $TMP

echo "Download $ZIPNAME"
wget -q $ZIPURL

echo "extract $ZIPNAME"
unzip -q $ZIPNAME

echo "list $ZIPNAME content"
ls -l 

echo "copy all to $BINDEST"
[ -d $BINDEST ] || sudo mkdir $BINDEST
sudo cp -r ./ $BINDEST
sudo chmod a+x $BINDEST/$BIN

echo "Create Gnome application"
cat > $APP <<EOF
[Desktop Entry]
Type=Application
Encoding=UTF-8
Name=Sieve
Comment=A Sieve management tool
Exec=$BINDEST/$BIN
#Icon=sieve.png
Terminal=false
EOF

echo "copy $APP to $APPDEST"
sudo cp $APP $APPDEST

cd -

rm -r $TMP

echo "Maintenant vous pouvez lancer Sieve depuis Gnome"
