# install Sieve

Simple script d'installation de l'application Sieve (https://github.com/thsmi/sieve)

Fonctionnement uniquement sur Gnome et dérivés

## étapes du script

1. Télécharge le paquet zip
1. Dézippe
1. Copie dans /usr/local/lib/sieve (nécessite les droits sudo) 
1. Active l'execution sur le binaire
1. Crée un fichier Sieve.desktop
1. Le place dans /usr/share/Applications (nécessite les droits sudo)

## Installation

```
$ wget https://framagit.org/alolise/install-sieve/-/archive/v0.4.0/install-sieve-v0.4.0.zip
$ unzip install-sieve-v0.4.0.zip
$ bash install-sieve-v0.4.0/install_sieve.sh
```

J'aurais aimé faire un truc classe, genre :

```
curl https://framagit.org/alolise/install-sieve | bash -
```

Mais j'ai pas trouvé comment :D

## toDo

1. proposer le choix : install system-wide / install for user
1. Trouver une icone sympa (y a R sur le github)
1. faire un truc classe à base de curl | bash :D
1. faire le suivi des versions (là il faut changer l'url dans le script)